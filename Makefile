all:
	@echo "Available targets: view, pull, push"

view:
	x-www-browser Activities.html

pull:
	scp mpii:/www/inf-resources-0/resources.mpi-inf.mpg.de/departments/d1/travel/Activities.html Activities.html

push:
	scp Activities.html mpii:/www/inf-resources-0/resources.mpi-inf.mpg.de/departments/d1/travel/Activities.html
